module cz.vse.java.adventura0915 {
    requires javafx.controls;
    requires javafx.fxml;


    opens cz.vse.java.adventura0915.main to javafx.fxml;
    exports cz.vse.java.adventura0915.main;
}